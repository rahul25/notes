package pl.rahul.notes.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.session.AppKeyPair;

import pl.rahul.notes.app.R;

import static android.content.SharedPreferences.Editor;

public class LoginActivity extends Activity
{
    final static private String APP_KEY = "hjwaony6nf9doim";
    final static private String APP_SECRET = "igyu67rddj6q9uj";

    private static DropboxAPI<AndroidAuthSession> _DBApi;

    private static final String ACCESS_TOKEN = "ACCESS_TOKEN";
    private static final String DROPBOX_INFO = "DROPBOX_INFO";
    private View.OnClickListener _loginButtonClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            _DBApi.getSession().startOAuth2Authentication(LoginActivity.this);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        AppKeyPair appKeys = new AppKeyPair(APP_KEY, APP_SECRET);
        AndroidAuthSession session = new AndroidAuthSession(appKeys);
        loadAccessToken(session);

        _DBApi = new DropboxAPI<AndroidAuthSession>(session);

        if (session.isLinked())
        {
            Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);
        }
        else
        {
            setContentView(R.layout.activity_login);
            View view = findViewById(R.id.login_via_dropbox);
            view.setOnClickListener(_loginButtonClickListener);
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        AndroidAuthSession dropboxAuthSession = _DBApi.getSession();
        if (dropboxAuthSession.authenticationSuccessful())
        {
            try
            {
                dropboxAuthSession.finishAuthentication();

                String accessToken = dropboxAuthSession.getOAuth2AccessToken();

                storeAuthToken(accessToken);

                Intent intent = new Intent(this, HomeActivity.class);
                startActivity(intent);

                Toast.makeText(this, "Dropbox Auth Successful with authtoken " + accessToken + " !!!", Toast.LENGTH_SHORT).show();
            }
            catch (IllegalArgumentException e)
            {
                Toast.makeText(this, "Dropbox Auth Failed !!!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void storeAuthToken(String authToken)
    {
        SharedPreferences sharedPreferences = getSharedPreferences(DROPBOX_INFO, 0);
        Editor editor = sharedPreferences.edit();
        editor.putString(ACCESS_TOKEN, authToken);
        editor.commit();
    }

    private void loadAccessToken(AndroidAuthSession authSession)
    {
        SharedPreferences sharedPreferences = getSharedPreferences(DROPBOX_INFO, 0);
        String accessToken = sharedPreferences.getString(ACCESS_TOKEN, null);
        if (accessToken != null)
        {
            authSession.setOAuth2AccessToken(accessToken);
        }
    }
}
